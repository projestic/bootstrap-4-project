## Gulp Sass 1.0 ##

#### 1. Setup Instructions:

   a. Clone
   ```
   git clone git@bitbucket.org:projestic/bootstrap-4-project.git
   ```

   c. Install dependencies
   ```
   npm install
   bower install
   ```

   d. Build project
   ```
   gulp